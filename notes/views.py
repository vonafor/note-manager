from django.views.generic.edit import CreateView
from django.views.generic.list import ListView
from django.views.generic.edit import UpdateView
from django.shortcuts import redirect, render_to_response
from django.http import Http404
from notes.models import Note
from notes.forms import NoteForm


class LoginRequiredMixin(object):

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return super(LoginRequiredMixin, self).dispatch(request, *args, **kwargs)
        else:
            return redirect('main')


class NoteCreateUpdateMixin(object):
    """
    CreateView и UpdateView имеют много общего
    """
    model = Note
    template_name = "notes/note_form.html"
    form_class = NoteForm

    def get_context_data(self, **kwargs):
        context = super(NoteCreateUpdateMixin, self).get_context_data(**kwargs)
        context['action'] = self.action
        return context


class NoteCreateView(LoginRequiredMixin, NoteCreateUpdateMixin, CreateView):
    action = "Create"

    def form_valid(self, form):
        instance = form.save(commit=False)
        instance.owner_id = self.request.user.id
        instance.save()
        return redirect('list note')


class NoteUpdateView(NoteCreateUpdateMixin, UpdateView):
    action = "Update"
    success_url = '/note/'

    def dispatch(self, request, *args, **kwargs):
        if self.request.user.id == self.get_object().owner_id:
            return super(NoteUpdateView, self).dispatch(request, *args, **kwargs)
        else:
            return redirect('main')


class NoteListView(LoginRequiredMixin, ListView):
    model = Note

    def get_queryset(self):
        queryset = Note.user_objects.for_user(self.request.user)
        return queryset


def note_content(request, uuid):
    """
    Отображает содержимое заметки в виде статичной страницы
    """
    try:
        note = Note.objects.get(uuid=uuid)
        if note.is_published:
            return render_to_response('notes/note.html', {'content': note.content})
    except (Note.DoesNotExist, ValueError):
        raise Http404(request)
    return redirect('main')


def ugly_decorator(func):
    """
    Клиент отправляет POST-запрос со списком заметок,
    к которым нужно применить определенное действие:
    добавить/убрать признак “избранная”, опубликовать/отменить публикацию,
    удалить
    """
    def wrapped(request):
        if request.POST:
            try:
                data = dict(request.POST).get('data[]', "")
                for d in map(int, data):
                    try:
                        note = Note.objects.get(id=d)
                        if request.user.id == note.owner_id:
                            func(note)
                    except Note.DoesNotExist:
                        continue
            except TypeError:
                pass
        return redirect('list note')
    return wrapped


@ugly_decorator
def make_favorite(note):
    note.favorite = True
    note.save()


@ugly_decorator
def unmake_favorite(note):
    note.favorite = False
    note.save()


@ugly_decorator
def delete_note(note):
    note.delete()


@ugly_decorator
def publish_note(note):
    note.is_published = True
    note.save()


@ugly_decorator
def unpublish_note(note):
    note.is_published = False
    note.save()
