from django.db import models
from django.contrib.auth.models import User
from uuid import uuid4


class Category(models.Model):
    name = models.CharField(max_length=25, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'categories'


class UserNoteManager(models.Manager):
    def for_user(self, user):
        return super(UserNoteManager, self).get_queryset().filter(owner=user)


class Note(models.Model):
    owner = models.ForeignKey(User)
    title = models.CharField(max_length=100)
    content = models.TextField()
    datetime = models.DateTimeField(auto_now_add=True)
    category = models.ForeignKey(Category, null=True, blank=True)
    favorite = models.BooleanField(default=False)
    uuid = models.UUIDField(default=uuid4, editable=True)
    is_published = models.BooleanField(default=False)

    objects = models.Manager()
    user_objects = UserNoteManager()

    class Meta:
        ordering = ['-datetime']

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return '/note/%s/' % self.uuid

    def is_favorite(self):
        return "Yes" if self.favorite else "No"
