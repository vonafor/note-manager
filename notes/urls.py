from django.conf.urls import url
from notes.views import NoteCreateView, NoteListView, NoteUpdateView
from notes.views import make_favorite, unmake_favorite, delete_note, publish_note, unpublish_note, note_content

urlpatterns = [
    url(r'^create/$', NoteCreateView.as_view(), name='create note'),
    url(r'^$', NoteListView.as_view(), name='list note'),
    url(r'^update/(?P<pk>\d+)$', NoteUpdateView.as_view(), name='update note'),
    url(r'^make_favorite/$', make_favorite, name='make favorite'),
    url(r'^unmake_favorite/$', unmake_favorite, name='unmake favorite'),
    url(r'^delete_note/$', delete_note, name='delete note'),
    url(r'^publish_note/$', publish_note, name='publish note'),
    url(r'^unpublish_note/$', unpublish_note, name='unpublish note'),
    url(r'^(?P<uuid>[a-z0-9-]+)/$', note_content),
]
