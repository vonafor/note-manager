from django.contrib import admin
from notes.models import Category, Note


class CategoryAdmin(admin.ModelAdmin):
    pass


class NoteAdmin(admin.ModelAdmin):
    list_display = ['owner', 'title', 'datetime', 'favorite']
    list_filter = ['datetime', 'title', 'category', 'favorite']


admin.site.register(Category, CategoryAdmin)
admin.site.register(Note, NoteAdmin)
