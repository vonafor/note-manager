from django import forms
from notes.models import Note


class NoteForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(NoteForm, self).__init__(*args, **kwargs)
        self.fields['title'].widget.attrs.update({'placeholder': 'Title'})
        self.fields['content'].widget.attrs.update({'placeholder': 'Type your text here'})

    class Meta:
        model = Note
        fields = ['title', 'category', 'favorite', 'content']
