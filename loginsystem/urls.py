from django.conf.urls import url
from loginsystem.views import RegisterFormView, LoginFormView, LogoutView

urlpatterns = [
    url(r'^$', LoginFormView.as_view(), name='main'),
    url(r'^register/$', RegisterFormView.as_view(), name='registration'),
    url(r'^logout/$', LogoutView.as_view(), name='logout'),
]
