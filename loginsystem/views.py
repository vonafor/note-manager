from django.views.generic.edit import FormView
from django.views.generic.base import View
from django.contrib.auth import login, logout
from django.http import HttpResponseRedirect
from loginsystem.forms import MyUserCreationForm, MyAuthenticationForm


class RegisterFormView(FormView):
    form_class = MyUserCreationForm
    success_url = "/"
    template_name = "register.html"

    def form_valid(self, form):
        form.save()
        return super(RegisterFormView, self).form_valid(form)


class LoginFormView(FormView):
    form_class = MyAuthenticationForm
    template_name = "main.html"
    success_url = "/note/"

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return HttpResponseRedirect('/note/')
        else:
            return super(LoginFormView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        user = form.get_user()
        login(self.request, user)
        return super(LoginFormView, self).form_valid(form)


class LogoutView(View):

    def dispatch(self, request, *args, **kwargs):
        logout(request)
        return HttpResponseRedirect("/")
